from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm



class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "to_do_lists_list"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    success_url = reverse_lazy("to_do_list_list")

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context["form"] = TodoListForm()
#         return context

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields =["name"]
    success_url = reverse_lazy("todo_list_list")

#     def get_success_url(self):
#         return reverse_lazy("todolist_detail", args=[self.object.listid])

# class TodoListUpdateView(UpdateView):
#     model = TodoList
#     template_name = "todos/update.html"

class TodoListDeleteView(DeleteView):
     model = TodoList
     template_name = "todos/delete.html"
     context_object_name = "to_do_delete_list"
     #success_url = reverse_lazy("todolist_list")

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    context_object_name = "to_do_edit_list"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items.html"
    fields = ["task", "due_date", "is_completed", "list"]
    context_object_name = "to_do_items_items"
    success_url = reverse_lazy("todo_list_list")

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    context_object_name = "to_do_edit_list"






    