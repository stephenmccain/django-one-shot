from django.urls import URLPattern, path

from todos.views import (
    
    TodoListListView, 
    TodoListCreateView,
    TodoListDetailView,
    TodoListUpdateView,
    TodoListDeleteView,
    TodoItemCreateView,
    TodoItemUpdateView,
    
    
    
)

urlpatterns = [

    path("", TodoListListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todolist_new"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_list_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todo_list_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todo_items_items"),
    path("<int:pk>items/edit/", TodoItemUpdateView.as_view(), name="todo_items_edit"),
    



]
