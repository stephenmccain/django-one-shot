from django import forms

from todos.models import TodoList

class TodoListForm(forms.ModelForm):
    model = TodoList
    fields = ["New Todo List"]